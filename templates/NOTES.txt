Congratulations! Your application has successfully been deployed to Kubernetes!

Open https://{{ .Values.ingress.hostname }}/ to test your installation!
