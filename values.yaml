# This file is part of the veraPDF Community Helm Chart
#    https://gitlab.com/MatthiasLohr/verapdf
#
# Copyright 2021-2022 Matthias Lohr <mail@mlohr.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

deployment:

  # Number of replicas to be deployed. When HorizontalPodAutoscaler is used, this number denoted the initial number of
  # Pods to be started.
  replicas: 1

  # Image to be used. We STRONGLY recommend using digest values, since verapdf/rest container registry does not provide
  # any tags beyond `latest`, which in our experience can be broken sometimes.
  # The value defaults to the last working image digest observed by us. If a new image is available, do not hesitate
  # to raise an issue at https://gitlab.com/MatthiasLohr/verapdf and ask for updating the default value. Thanks!
  image: verapdf/rest:v1.24.1
  imagePullPolicy: IfNotPresent

ingress:
  # Should an Ingress resource be created? Disable if you only want to use veraPDF from within your cluster.
  enable: true

  # Please change this to the domain you want to use for your veraPDF setup.
  hostname: demo.verapdf.org

  class: "nginx"

  # Some useful default annotations:
  # - We strongly recommend using TLS
  # - In order to be able to upload bigger PDF files for checking, we need nginx to allow for bigger proxy body sizes
  annotations:
    kubernetes.io/tls-acme: "true"
    nginx.ingress.kubernetes.io/proxy-body-size: "256m"

horizontalPodAutoscaler:
  # Should a HorizontalPodAutoscaler be created?
  enable: true

  # Minimal replica count
  minReplicas: 1

  # Maximal replica count
  maxReplicas: 10
  
  # Limit of overall CPU utilization before a new Pod gets started
  targetCPUUtilizationPercentage: 75
