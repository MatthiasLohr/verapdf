# veraPDF

This repository contains a community Helm Chart for [veraPDF](https://verapdf.org/), to quickly set up your own veraPDF REST validation service similar to https://demo.verapdf.org/.


## Installation

### Add Helm Repository

```
helm repo add mlohr https://helm-charts.mlohr.com/
helm repo update
```


### Install to Kubernetes

```
helm install verapdf mlohr/verapdf --set ingress.hostname=verapdf.yourdomain.org --wait
```


### Configuration

Please check [values.yaml](https://gitlab.com/MatthiasLohr/verapdf/-/blob/main/values.yaml) for configuration options.
Use `helm --set` to change individual values or `helm -f` to provide a custom values.yaml file.


## Validate PDF Files

### Manual Validation

Open https://demo.verapdf.org/ (or use the URL from your own installation) in your browser and upload your PDF.


### Automatic Validation

You can use the following bash commands to check your PDF file against veraPDF:
```bash
curl -F "file=@/path/to/yourdocument.pdf" -H "Accept: application/json" https://demo.verapdf.org/api/validate/1b > verapdf-validation-results.json
if [ "$(cat verapdf-validation-results.json | jq .compliant)" != "true" ] ; then exit 1 ; fi
```
This can for example also be included in a CI/CD pipeline.
Please replace `https://demo.verapdf.org/` with your own installation's URL.


## Official veraPDF Resources

  * https://verapdf.org -- official veraPDF website
  * https://demo.verapdf.org/ -- veraPDF demo webservice
  * https://github.com/veraPDF/ -- veraPDF GitHub space


## License

This helm chart is published under the Apache License, Version 2.0.
See [LICENSE.md](https://gitlab.com/MatthiasLohr/verapdf/-/blob/master/LICENSE.md) for more information.

Copyright (c) by [Matthias Lohr](https://mlohr.com/) &lt;[mail@mlohr.com](mailto:mail@mlohr.com)&gt;
