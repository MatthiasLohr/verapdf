# Changelog

## v1.2.2

  * Upgraded veraPDF to v1.24.1
  * Upgraded `bitnami/common` dependency to v2.12.0

## v1.2.1

  * Upgraded veraPDF to v1.22.2
  * Upgraded `bitnami/common` dependency to v2.2.4

## v1.2.0

  * Upgraded veraPDF to v1.20.1
  * Upgraded `bitnami/common` dependency to v1.11.1

## v1.1.0

  * Added license and signing information
  * Added `NetworkPolicy` to block all outgoing traffic from this Pod (PDF files can be dangerous!)
  * Use bitnami/common chart

## v1.0.0

  * First release with veraPDF-rest v1.18.8
